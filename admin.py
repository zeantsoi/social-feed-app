from copy import deepcopy
from django.contrib import admin
from adrenaline.social import models


class SocialFeedAdmin(admin.ModelAdmin):
	"""Admin display for social feeds."""
    list_display = ('handle','service','priority')
    list_filter = ('service','priority')


class SocialHashtagAdmin(admin.ModelAdmin):
	"""Admin display for hashtags."""
    list_display = ('tag',)


admin.site.register(models.Feed, SocialFeedAdmin)
admin.site.register(models.Hashtag, SocialHashtagAdmin)