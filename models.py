from django.db import models

PRIORITIES = (
	('automatic', 'Automatic'),
	('hashtags', 'Select Hashtags'),
)

SERVICES = (
	('twitter', 'Twitter'),
	('instagram', 'Instagram'),
)


class Feed(models.Model):
    """Model for each feed to be displayed"""
    handle = models.CharField(max_length=100)
    service = models.CharField(max_length=40, choices=SERVICES, blank=False)
    priority = models.CharField(max_length=40, choices=PRIORITIES, blank=False)

    class Meta:
        verbose_name = u'feed'
        verbose_name_plural = u'feeds'

    def __unicode__(self):
        return self.handle + '_' + self.service + '_' + self.priority


class Hashtag(models.Model):
    """Model for each hashtags to be polled"""
    tag = models.CharField(max_length=100, help_text="Please enter without preceding '#'")

    class Meta:
        verbose_name = u'hashtag'
        verbose_name_plural = u'hashtags'

    def __unicode__(self):
        return self.tag
