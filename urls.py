from django.conf.urls.defaults import *

urlpatterns = patterns("adrenaline.social.views",
    url('^expand/', "expand", name='social_expand'),
)