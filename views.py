import datetime
import pytz
import re

from django.conf import settings
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from instagram.client import InstagramAPI
from mezzanine.blog.models import BlogPost
import twitter

from adrenaline.events.models import Events

#########################
# SOCIAL GLOBALS #
#########################
# Inserted as class in HTML tag for each respective feed
TWITTER_SIZE = ["size2", "size2", "size1", "size3", "size2", "size1", "size2", "size2", "size3", "size1", "size2"]
TWITTER_COLOR = ["color2", "color1", "color3", "color1", "color2", "color3", "color1", "color2", "color1", "color3",
                 "color1"]

INSTAGRAM_SIZE = ["size3", "size3", "size1", "size2", "size4", "size3", "size3", "size1", "size4", "size1", "size3",
                  "size4"]
INSTAGRAM_COLOR = ["color1", "color3", "color1", "color3", "color1", "color3", "color2", "color1", "color2", "color1",
                   "color3", "color3"]

BLOG_SIZE = ["size2", "size2", "size1", "size3"]
BLOG_COLOR = ["color2", "color1", "color2", "color1"]

SOCIAL_FEED_CLASSES = [{
                           'twitter_size': TWITTER_SIZE,
                           'twitter_color': TWITTER_COLOR,
                           'instagram_size': INSTAGRAM_SIZE,
                           'instagram_color': INSTAGRAM_COLOR,
                           'blog_size': BLOG_SIZE,
                           'blog_color': BLOG_COLOR,
                       }]

def expand(request):
    """Returns HTML markup for expanded feed.
    Retrieves feed based on GET variables.
    ONLY RETURNS MARKUP FOR INSTAGRAM FEEDS.
    """
    service, request_id = None, None
    if 'service' in request.GET:
        service = request.GET['service']
    if 'id' in request.GET:
        request_id = request.GET['id']
    # If service is Instagram, continue
    if service == 'instagram_item':
        api = InstagramAPI(access_token=settings.INSTAGRAM_ACCESS_TOKEN)
        markup = ''
        try:
            media = api.media(request_id)
            markup += '<div id="bg_cover"><div class="instagram_popup"><img src="' + media.images[
                'standard_resolution'].url + '" alt="Instagram Photo"/><p class="inDate">' + media.created_time.strftime(
                "%B %d, %Y") + '</p><p class="inCaption">' + media.user.username + ': ' + media.caption.text + '</p><p class="inLikes">' + str(
                media.like_count) + ' Likes</p><ul>';
            # Append markup for each comment
            for comment in media.comments:
                markup += '<li><strong>@' + comment.user.username + '</strong>: ' + comment.text + '</li>'

            markup += '</ul><a href="#closeModal" class="simplemodal-close"><img src="/static/images/close_button.png" /></div>' 
        # If exception, empty string is returned
        except:
            pass

    else:
        markup = ''

    return HttpResponse(markup, mimetype="text/plain")


class Feeds(object):
    """Returns a list of dicts containing feed information.
    Sorted by current page, page hashtags, and page categories.
    """
    def __init__(self, page):
        """Initializes instance with page parameters."""
        self.page = page
        self.hashtags = self.page.hashtags.all()
        self.blog_categories = self.page.blog_categories.all()

    def all(self):
        """Collates across all feeds and returns as a list."""
        feeds = []
        feeds.extend(self.twitter())
        feeds.extend(self.instagram())
        feeds.extend(self.blog())
        return feeds

    def twitter(self):
        """Returns dict for each Twitter feed that satisfy page parameters."""
        # Get a list of all Twitter accounts associated with this page
        handles = self.handles('twitter')

        # Initialize Twitter API with credentials
        api = twitter.Api(consumer_key=settings.TWITTER_CONSUMER_KEY,
                          consumer_secret=settings.TWITTER_CONSUMER_SECRET,
                          access_token_key=settings.TWITTER_ACCESS_TOKEN_KEY,
                          access_token_secret=settings.TWITTER_ACCESS_TOKEN_SECRET)

        # Get a list of all tweets matching the page's hashtags
        tweets = []
        for hashtag in self.hashtags:
            try:
                tweets.extend(api.GetSearch('#' + str(re.sub('[#]', '', hashtag.tag))))
            except:
                pass

        # Ensure that the tweet's user is authorized to be posted on this page
        approved_tweets = []
        for tweet in tweets:
            # Add tweet if username is in restricted handles
            if tweet.user.screen_name in handles['restricted']:
                approved_tweets.append(tweet)

        # Add all tweets for unrestricted handles
        for handle in handles['unrestricted']:
            try:
                for tweet in api.GetUserTimeline(handle):
                    approved_tweets.append(tweet)
            except:
                pass

        # Return list of all valid tweets
        return self.feed_list(approved_tweets, 'twitter')

    def instagram(self):
        """Returns dict for each Instagram feed that satisfy page parameters."""
        # Get a list of all Instagram accounts associated with this page
        handles = self.handles('instagram')

        # Get a list of all Instagrams matching the specified hashtag
        instagrams = []
        # Initialize OAuth API queries
        api = InstagramAPI(access_token=settings.INSTAGRAM_ACCESS_TOKEN)

        for hashtag in self.hashtags:
            try:
                instagrams.extend(api.tag_recent_media(tag_name=re.sub('[#]', '', hashtag.tag))[0])
            except:
                instagrams.extend({})

        # Ensure that the Instagram's user is authorized to be posted on this page
        approved_instagrams = []
        for instagram in instagrams:
            if instagram.user.username in handles['restricted']:
                approved_instagrams.append(instagram)

        # Add all Instagrams for unrestricted handles
        for handle in handles['unrestricted']:
            # Get Instagram user_id from handle
            try:
                user_id = api.user_search(handle)[0].id
                for instagram in api.user_recent_media(user_id=user_id)[0]:
                    approved_instagrams.append(instagram)
            except:
                approved_instagrams.append({})

        # Return list of all valid Instagrams
        return self.feed_list(approved_instagrams, 'instagram')

    def blog(self):
        """Returns all blog feeds matching the page_categories associated with the page."""
        return self.feed_list(BlogPost.objects.filter(categories__in=self.blog_categories).distinct(), 'blog')

    def event(self):
        """Returns all events matching the event_categories associated with the page."""
        events_categories = self.page.events_categories.all()
        return self.feed_list(Events.objects.filter(category__in=events_categories).distinct(), 'event')

    def handles(self, service):
        """Returns all handles associated with the current page, by service.
        Unrestricted handles will have all their posts published.
        Restricted handles will have their posts published ONLY
        if the posts' hashtags satisfy the page's requirements.
        """
        restricted_handles, unrestricted_handles = [], []
        for feed in self.page.feeds.filter(service=service).all():
            if feed.priority == 'automatic':
                unrestricted_handles.append(str(feed.handle))
            elif feed.priority == 'hashtags':
                restricted_handles.append(str(feed.handle))
        return {'unrestricted': unrestricted_handles, 'restricted': restricted_handles}


    def feed_list(self, feeds, service):
        """Return a dict that includes all the information we need, including 
        1) service
        2) datetime
        3) extras
        4) position (metadata for styling)
        5) size (metadata for styling)
        6) color (metadata for styling)
        """
        feed_list = []
        for index, feed in enumerate(feeds):
            if service == 'event':
                timestamp = datetime.datetime(feed.start_date.year, feed.start_date.month, feed.start_date.day)
                feed_list.append({'service': service, 'datetime': timestamp, 'extras': feed})
            else:
                if service == 'twitter':
                    timestamp = datetime.datetime.fromtimestamp(feed.created_at_in_seconds)
                    position = 'position' + str(index + 1)
                    if index <= len(TWITTER_SIZE) - 1:
                        size = TWITTER_SIZE[index]
                        color = TWITTER_COLOR[index]                        
                elif service == 'instagram':
                    try:    
                        timestamp = feed.created_time
                    except:
                        timestamp = None
                    position = 'position' + str(index + 1)
                    if index <= len(INSTAGRAM_SIZE) - 1:
                        size = INSTAGRAM_SIZE[index]
                        color = INSTAGRAM_COLOR[index]
                elif service == 'blog':
                    timestamp = feed.publish_date
                    position = 'position' + str(index + 1)
                    if index <= len(BLOG_SIZE) - 1:
                        size = BLOG_SIZE[index]
                        color = BLOG_COLOR[index]
                
                # Only add to list if is within the range of the class constant
                if size and color:
                    feed_list.append({'service': service, 'datetime': timestamp, 'extras': feed, 'class': {'position': position, 'size': size, 'color': color}})

                # Reset these values for re-use in the conditional above
                size, color = None, None

        # If there aren't enough feeds to fill the page, fill the placeholders with default data
        if service == 'twitter':        
            for index in range(len(TWITTER_SIZE)):
                self.fill_if_empty(index, feed_list, service)
        elif service == 'instagram':        
            for index in range(len(INSTAGRAM_SIZE)):
                self.fill_if_empty(index, feed_list, service)
        elif service == 'blog':     
            for index in range(len(BLOG_SIZE)):
                self.fill_if_empty(index, feed_list, service)

        return feed_list

    def fill_if_empty(self, index, feed_list, service):
        """Fills any empty feed positions with default styling.
        First, checks whether an object exists at the index.
        Then, applies default styling and data if empty.
        Finally, feed_list – whether altered or not – is returned."""
        try: 
            feed_list[index]
        except IndexError:
            if service == 'twitter':
                style = {
                    'color': TWITTER_COLOR[index],
                    'size': TWITTER_SIZE[index],
                }
            if service == 'instagram':
                style = {
                    'color': INSTAGRAM_COLOR[index],
                    'size': INSTAGRAM_SIZE[index],
                }
            if service == 'blog':
                style = {
                    'color': BLOG_COLOR[index],
                    'size': BLOG_SIZE[index],
                }

            style.update({'position': 'position' + str(index + 1)})
            feed_list.append({'datetime': datetime.datetime(1900, 1, 1), 'extras': None, 'class': style})
        
        return feed_list