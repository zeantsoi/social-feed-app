import urllib
from datetime import date

from cartridge.shop.models import Product
from django.core.cache import cache
from mezzanine.blog.models import BlogPost
from mezzanine.conf import settings
from mezzanine.pages.page_processors import processor_for

from adrenaline.social import views as social

@processor_for('awareness')
def awareness_page(request, page):
    """Creates context for the awareness page.
    Social feeds are cached and updated once per hour (3600).
    """
    annual_events = event.AnnualEvent.objects.filter(awareness_page_id=page.id).order_by('month')
    goals = custom_pages.Goal.objects.filter(awareness_page_id=page.id)

    feeds = social.Feeds(custom_pages.AwarenessPage.objects.get(page_ptr=page))
    if not cache.get('twitter_lacrosse'):
        cache.set('twitter_lacrosse', sorted(feeds.twitter(), key=lambda k: k['datetime'], reverse=True), 3600)
    if not cache.get('instagram_lacrosse'):
        cache.set('instagram_lacrosse', sorted(feeds.instagram(), key=lambda k: k['datetime'], reverse=True), 3600)
    context = {
        'twitter': cache.get('twitter_lacrosse'),
        'instagram': cache.get('instagram_lacrosse'),
        'blog': sorted(feeds.blog(), key=lambda k: k['datetime'], reverse=True),
        'video_url': page.awarenesspage.video_url.split('/')[-1],
        'annual_events': annual_events,
        'goals': goals,
    }
    
    return context