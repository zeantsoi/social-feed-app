# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Feed'
        db.create_table('social_feed', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('_order', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('handle', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('service', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('priority', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal('social', ['Feed'])

        # Adding model 'Hashtag'
        db.create_table('social_hashtag', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('_order', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('tag', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('social', ['Hashtag'])


    def backwards(self, orm):
        # Deleting model 'Feed'
        db.delete_table('social_feed')

        # Deleting model 'Hashtag'
        db.delete_table('social_hashtag')


    models = {
        'social.feed': {
            'Meta': {'ordering': "('_order',)", 'object_name': 'Feed'},
            '_order': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'handle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'service': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'social.hashtag': {
            'Meta': {'ordering': "('_order',)", 'object_name': 'Hashtag'},
            '_order': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['social']